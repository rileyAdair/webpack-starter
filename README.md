# Webpack Starter

A light webpack starter for front-end projects.

### Installation

```
npm install
```

### Start Development Server

```
npm start
```

Project will be running at http://localhost:8080/.

### Build Production Version

```
npm run build
```

The output of the build will be exposed inside the **public** folder.

### Features:

* ES6 Support via [babel](https://babeljs.io/) (v7)
* SASS Support via [sass-loader](https://github.com/jtangelder/sass-loader)
* Linting via [eslint-loader](https://github.com/MoOx/eslint-loader)

**npm run build** uses [mini-css-extract-plugin](https://github.com/webpack-contrib/mini-css-extract-plugin) to move the css to a separate file. The css file gets included in the head of the **index.html**. The js is included as the last tag in the body.

#### To-do

* Incorporate file-loader / url-loader to hash image paths and image assets in **static** folder.
* Loop over all **.html** files in **src/templates** folder for html-webpack-plugin. (currently have to manually add new templates in **webpack.common.js**)

#### Flash of Unstyled Content

If the "Flash of Unstyled Content" during development is troublesome for you on page load, copy the **MiniCssExtractPlugin** setup from **webpack.config.prod.js** and replace 'style-loader' in **webpack.config.dev.js**. This flicker "happens because the browser takes a while to load JavaScript and the styles would be applied only then." Read about it [here](https://survivejs.com/webpack/styling/separating-css/).
