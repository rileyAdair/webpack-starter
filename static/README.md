### Static

Every directory and file in the **static** folder will be copied into the **public** by [copy-webpack-plugin](https://github.com/webpack-contrib/copy-webpack-plugin).

E.g. if you add a file named **sun.jpg** to the static folder, it'll be copied to **public/sun.jpg**.

```scss
// CSS usage

.sun {
  background-image: url('/sun.jpg');
}
```